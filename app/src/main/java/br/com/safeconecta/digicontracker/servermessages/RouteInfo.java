package br.com.safeconecta.digicontracker.servermessages;

import android.graphics.Color;

/**
 * Created by Rafael on 17/02/2017.
 */

public class RouteInfo extends ServerMessage {

    private String text;
    private int color;

    public RouteInfo(String text, String colorHex) {
        super(TYPE_ROUTE_INFO);
        this.text = text;
        color = Color.parseColor("#" + colorHex);
    }

    public String getText() {
        return text;
    }

    public int getColor() {
        return color;
    }
}
