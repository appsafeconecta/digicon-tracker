package br.com.safeconecta.digicontracker.tracking;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.safeconecta.digicontracker.R;
import br.com.safeconecta.digicontracker.clientmessages.TrackedLocation;
import br.com.safeconecta.digicontracker.services.mainservice.IMainService;
import br.com.safeconecta.digicontracker.services.mainservice.MainServiceStub;
import br.com.safeconecta.digicontracker.services.mainservice.ServerResponse;

/**
 * Created by Rafael on 17/02/2017.
 */

public class TrackerThread extends Thread {

    private static final String TAG = "TrackerThread";
    private static final long sendInterval = 30000;

    private Context context;
    private long timeToSend;
    private Location location;
    private List<TrackedLocation> messageQueue;
    private IMainService webService;

    public TrackerThread(Context context) {
        this.context = context;
        timeToSend = System.currentTimeMillis();
        messageQueue = new ArrayList<>();
        webService = new MainServiceStub();
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public void run() {
        while (true) {
            long currentTime = System.currentTimeMillis();
            if (currentTime >= timeToSend) {
                timeToSend = currentTime + sendInterval;
                if (location != null) {
                    TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                    String imei = telephonyManager.getDeviceId();
                    TrackedLocation trackedLocation = new TrackedLocation(new Date(), imei, location.getLatitude(), location.getLongitude(), location.getSpeed(), location.getBearing());
                    sendToWebService(trackedLocation);
                } else {
                    Log.d(TAG, "Tried to sendToWebService but location is null!");
                }

                if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO sendToWebService message to web service warning about denied permissions
                    Log.w(TAG, "Warning: Permissions are not granted!");
                }

            }
        }
    }

    private void sendToWebService(TrackedLocation trackedLocation) {
        Log.d(TAG, "Sending " + trackedLocation.getJsonString());
        ServerResponse response = webService.sendLocation(trackedLocation);
        Log.d(TAG, "error? " + response.isError() + ", numMsgs=" + response.getMessages().size());
        if (response.isError()) {
            Log.d(TAG, "Adding to queue " + trackedLocation.getJsonString());
            messageQueue.add(trackedLocation);
        } else {

            sendBroadcast(response);

            if (messageQueue.size() > 0) {
                List<TrackedLocation> tempQueue = new ArrayList<>(messageQueue);
                messageQueue.clear();
                while (tempQueue.size() > 0) {
                    Log.d(TAG, "Removing from temp queue");
                    sendToWebService(tempQueue.remove(0));
                }
            }
        }
    }

    private void sendBroadcast(ServerResponse response) {
        Log.d(TAG, "sendBroadcast " + response.toString());
        Intent intent = new Intent(context.getString(R.string.broadcast_action_key));
        intent.putExtra(context.getString(R.string.data_main_service_response_key), response.toString());
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

}
