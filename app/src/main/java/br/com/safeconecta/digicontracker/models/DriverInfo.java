package br.com.safeconecta.digicontracker.models;

/**
 * Created by Rafael on 05/04/2017.
 */

public class DriverInfo {

    private String register;
    private String name;

    public DriverInfo(String register, String name) {
        this.register = register;
        this.name = name;
    }

    public String getRegister() {
        return register;
    }

    public String getName() {
        return name;
    }
}
