package br.com.safeconecta.digicontracker.services.mainservice;


import java.util.ArrayList;
import java.util.List;

import br.com.safeconecta.digicontracker.servermessages.AccessibilityInfo;
import br.com.safeconecta.digicontracker.servermessages.NearbyCars;
import br.com.safeconecta.digicontracker.servermessages.RouteInfo;
import br.com.safeconecta.digicontracker.servermessages.ServerMessage;

/**
 * Created by Rafael on 17/02/2017.
 */

public class ServerResponse {

    private static final String TAG = "ServerResponse";

    private static final String MESSAGE_SEPARATOR = "#";
    private static final String PARAMETER_SEPARATOR = "@";
    private static final String ERROR = "XX";

    private static final String ROUTE_INFO = "RI";
    private static final String NEARBY_CARS = "NC";
    private static final String ACCESSIBILITY_INFO = "AI";

    private String responseString;
    private boolean error;
    private List<ServerMessage> messages;

    public ServerResponse(String responseStr) {
        this.responseString = responseStr;
        if (ERROR.equals(responseStr)) {
            error = true;
            messages = new ArrayList<>();
        } else {
            error = false;
            messages = new ArrayList<>();
            String[] messagesStr = responseStr.split(MESSAGE_SEPARATOR);
            for (String m : messagesStr) {
                messages.add(createServerMessage(m));
            }
        }
    }

    public ServerResponse(boolean error) {
        this.error = error;
        this.messages = new ArrayList<>();
    }

    private ServerMessage createServerMessage(String message) {
        String[] params = message.split(PARAMETER_SEPARATOR);
        if (ROUTE_INFO.equals(params[0])) {
            String text = params[1];
            String colorHex = params[2];
            return new RouteInfo(text, colorHex);
        } else if (NEARBY_CARS.equals(params[0])) {
            String previousId = params[1];
            int previousDistance = Integer.parseInt(params[2]);
            int previousTime = Integer.parseInt(params[3]);
            String nextId = params[4];
            int nextDistance = Integer.parseInt(params[5]);
            int nextTime = Integer.parseInt(params[6]);
            return new NearbyCars(previousId, previousDistance, previousTime, nextId, nextDistance, nextTime);
        } else if (ACCESSIBILITY_INFO.equals(params[0])) {
            String stop = params[1];
            int time = Integer.parseInt(params[2]);
            return new AccessibilityInfo(stop, time);
        }
        return null;
    }

    public boolean isError() {
        return error;
    }

    public List<ServerMessage> getMessages() {
        return messages;
    }

    @Override
    public String toString() {
        return responseString;
    }
}
