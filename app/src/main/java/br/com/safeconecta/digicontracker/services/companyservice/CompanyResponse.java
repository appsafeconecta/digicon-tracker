package br.com.safeconecta.digicontracker.services.companyservice;


import br.com.safeconecta.digicontracker.models.CompanyParameters;

/**
 * Created by Rafael on 04/04/2017.
 */

public class CompanyResponse {

    private boolean error;
    private CompanyParameters parameters;

    public CompanyResponse(CompanyParameters parameters) {
        this.error = false;
        this.parameters = parameters;
    }

    public CompanyResponse(boolean error) {
        this.error = error;
    }

    public boolean isError() {
        return error;
    }

    public CompanyParameters getParameters() {
        return parameters;
    }
}
