package br.com.safeconecta.digicontracker;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;

/**
 * Created by Rafael on 05/04/2017.
 */

public class Properties {

    private static final String TAG = "Properties";

    public static void writeObject(Context context, String key, Object obj) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(context.getString(R.string.preferences_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        editor.putString(key, gson.toJson(obj));
        editor.commit();
        Log.d(TAG, "Persisted " + key + ": " + sharedPreferences.getString(key, ""));
    }

    public static <T> T readObject(Context context, String key, Class<T> objClass) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(context.getString(R.string.preferences_file_key), Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(key, "");
        Log.d(TAG, "Retrieved " + key + ": " + json);
        return gson.fromJson(json, objClass);
    }

    public static void clear(Context context){
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(context.getString(R.string.preferences_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
        Log.d(TAG, "Cleared Preferences");
    }

}
