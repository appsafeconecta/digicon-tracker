package br.com.safeconecta.digicontracker.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import br.com.safeconecta.digicontracker.Properties;
import br.com.safeconecta.digicontracker.R;
import br.com.safeconecta.digicontracker.models.CompanyParameters;
import br.com.safeconecta.digicontracker.services.companyservice.CompanyResponse;
import br.com.safeconecta.digicontracker.services.companyservice.CompanyServiceStub;
import br.com.safeconecta.digicontracker.services.companyservice.ICompanyService;


/**
 * Created by Rafael on 04/04/2017.
 */

public class ParametersActivity extends AppCompatActivity {

    private static final String TAG = "ParametersActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_parameters);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Thread thread = new Thread() {
            @Override
            public void run() {
                CompanyParameters companyParameters = Properties.readObject(ParametersActivity.this, getString(R.string.prefs_company_parameters), CompanyParameters.class);
                if (companyParameters != null) {
                    goToNextActivity(companyParameters);
                } else {
                    ICompanyService companyService = new CompanyServiceStub();
                    CompanyResponse response = companyService.getCompanyParameters("RIOITA");
                    if (response.isError()) {
                        // TODO redirect to register code activity
                    } else {
                        Properties.writeObject(ParametersActivity.this, getString(R.string.prefs_company_parameters), response.getParameters());
                        goToNextActivity(response.getParameters());
                    }
                }
            }
        };
        thread.start();
    }

    private void goToNextActivity(CompanyParameters companyParameters) {
        startActivity(new Intent(this, TrackingActivity.class));
        finish();
    }
}
