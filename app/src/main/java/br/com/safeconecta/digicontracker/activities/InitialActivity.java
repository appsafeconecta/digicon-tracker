package br.com.safeconecta.digicontracker.activities;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import br.com.safeconecta.digicontracker.MyDeviceAdminReceiver;


/**
 * Created by Rafael on 05/04/2017.
 */

public class InitialActivity extends AppCompatActivity {

    private static final int REQUEST_CODE = 0;
    private DevicePolicyManager devicePolicyManager;
    private ComponentName adminName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            devicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
            adminName = new ComponentName(this, MyDeviceAdminReceiver.class);
            if (!devicePolicyManager.isAdminActive(adminName)) {
                Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
                intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, adminName);
                intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "Ative as permissões de administrador para que o aplicativo funcione corretamente.");
                startActivityForResult(intent, REQUEST_CODE);
            } else {
                proceed();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (REQUEST_CODE == requestCode) {
            if (resultCode == Activity.RESULT_OK) {
                proceed();
            } else {
                finish();
            }
        }
    }

    private void proceed() {
        startActivity(new Intent(this, ParametersActivity.class));
        finish();
    }
}
