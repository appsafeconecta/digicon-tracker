package br.com.safeconecta.digicontracker.servermessages;

/**
 * Created by Rafael on 17/02/2017.
 */

public abstract class ServerMessage {

    public static final int TYPE_ROUTE_INFO = 1;
    public static final int TYPE_NEARBY_CARS = 2;
    public static final int TYPE_ACCESSIBILITY_INFO = 3;

    private int type;

    public ServerMessage(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
