package br.com.safeconecta.digicontracker.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import br.com.safeconecta.digicontracker.R;


/**
 * Created by Rafael on 06/04/2017.
 */

public class NearbyMap extends View {

    public static final int DIRECTION_LEFT = 0;
    public static final int DIRECTION_RIGHT = 1;

    private float totalWidth;
    private float mapWidth;
    private float mapHeight;

    private float ratio;
    private int direction;
    private float busSize;
    private float arrowSize;

    private float busY;
    private Paint ownBusPaint;
    private Paint otherBusPaint;
    private Bitmap busBitmap;
    private Rect busBitmapSrc;
    private RectF ownBusBounds;
    private RectF leftBusBounds;
    private RectF rightBusBounds;
    private Bitmap arrowBitmap;
    private Rect arrowBitmapSrcRight;
    private Rect arrowBitmapSrcLeft;
    private Paint arrowPaint;
    private RectF[] arrowBounds;

    public NearbyMap(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.NearbyMap,
                0, 0);

        try {
            ratio = a.getFloat(R.styleable.NearbyMap_ratio, 0.5f);
            direction = a.getInteger(R.styleable.NearbyMap_direction, DIRECTION_LEFT);
            busSize = a.getDimension(R.styleable.NearbyMap_busSize, 32f);
            arrowSize = a.getDimension(R.styleable.NearbyMap_arrowSize, 32f);
        } finally {
            a.recycle();
        }

        init();
    }

    private void init() {
        busBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_bus);
        busBitmapSrc = new Rect(0, 0, busBitmap.getWidth(), busBitmap.getHeight());
        ownBusPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        ownBusPaint.setStyle(Paint.Style.FILL);
        ownBusPaint.setColorFilter(new PorterDuffColorFilter(Color.BLUE, PorterDuff.Mode.SRC_IN));
        otherBusPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        otherBusPaint.setStyle(Paint.Style.FILL);
        otherBusPaint.setColorFilter(new PorterDuffColorFilter(Color.BLACK, PorterDuff.Mode.SRC_IN));
        ownBusBounds = new RectF();
        leftBusBounds = new RectF();
        rightBusBounds = new RectF();
        arrowBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_arrow);
        arrowBitmapSrcRight = new Rect(0, 0, arrowBitmap.getWidth(), arrowBitmap.getHeight());
        arrowBitmapSrcLeft = new Rect(arrowBitmap.getWidth(), arrowBitmap.getHeight(), 0, 0);
        arrowPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        arrowPaint.setStyle(Paint.Style.FILL);
        arrowPaint.setColorFilter(new PorterDuffColorFilter(Color.BLACK, PorterDuff.Mode.SRC_IN));
        arrowPaint.setAlpha(128);
        arrowBounds = new RectF[6];
        for (int i = 0; i < arrowBounds.length; i++)
            arrowBounds[i] = new RectF();
    }

    public void setRatio(float ratio) {
        this.ratio = ratio;
        invalidate();
    }

    public void toggleDirection() {
        if (direction == DIRECTION_LEFT)
            direction = DIRECTION_RIGHT;
        else
            direction = DIRECTION_LEFT;
        invalidate();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        float xpad = (float) (getPaddingLeft() + getPaddingRight());
        float ypad = (float) (getPaddingTop() + getPaddingBottom());
        totalWidth = w;
        mapWidth = (float) w - xpad - busSize;
        mapHeight = (float) h - ypad;
        busY = mapHeight / 2;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        float ownBusX;
        if (direction == DIRECTION_LEFT) {
            ownBusX = getPaddingLeft() + busSize / 2 + (1 - ratio) * mapWidth;
        } else {
            ownBusX = getPaddingLeft() + busSize / 2 + ratio * mapWidth;
        }
        ownBusBounds.set(ownBusX - busSize / 2, busY - busSize / 2, ownBusX + busSize / 2, busY + busSize / 2);
        leftBusBounds.set(getPaddingLeft(), busY - busSize / 2, busSize, busY + busSize / 2);
        rightBusBounds.set(totalWidth - getPaddingRight() - busSize, busY - busSize / 2, totalWidth - getPaddingRight(), busY + busSize / 2);
        for (int i = 0; i < arrowBounds.length; i++) {
            float y = mapHeight / 2;
            float x = getPaddingLeft() + busSize + arrowSize / 2 + i * ((totalWidth - busSize * 2 - arrowSize) / (arrowBounds.length - 1));
            arrowBounds[i].set(x - arrowSize / 2, y - arrowSize / 2, x + arrowSize / 2, y + arrowSize / 2);
        }

        for (RectF arrowBound : arrowBounds)
            canvas.drawBitmap(arrowBitmap, direction == DIRECTION_LEFT ? arrowBitmapSrcLeft : arrowBitmapSrcRight, arrowBound, arrowPaint);
        canvas.drawBitmap(busBitmap, busBitmapSrc, ownBusBounds, ownBusPaint);
        canvas.drawBitmap(busBitmap, busBitmapSrc, leftBusBounds, otherBusPaint);
        canvas.drawBitmap(busBitmap, busBitmapSrc, rightBusBounds, otherBusPaint);
    }
}
