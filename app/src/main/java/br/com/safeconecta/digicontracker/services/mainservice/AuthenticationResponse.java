package br.com.safeconecta.digicontracker.services.mainservice;

/**
 * Created by Rafael on 04/04/2017.
 */

public class AuthenticationResponse {

    private static final String ERROR = "%";

    private String responseString;
    private boolean error;
    private String driverName;

    public AuthenticationResponse(String responseStr) {
        this.responseString = responseStr;
        if (ERROR.equals(responseStr)) {
            error = true;
        } else {
            error = false;
            driverName = responseStr;
        }
    }

    public AuthenticationResponse(boolean error) {
        this.error = error;
    }

    public boolean isError() {
        return error;
    }

    public String getDriverName() {
        return driverName;
    }
}
