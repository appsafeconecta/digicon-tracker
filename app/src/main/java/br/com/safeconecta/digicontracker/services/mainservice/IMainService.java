package br.com.safeconecta.digicontracker.services.mainservice;


import br.com.safeconecta.digicontracker.clientmessages.TrackedLocation;

/**
 * Created by Rafael on 17/02/2017.
 */

public interface IMainService {

    public ServerResponse sendLocation(TrackedLocation trackedLocation);
    public AuthenticationResponse authenticate(String register);
    public void startDrive(String register, String imei);
}
