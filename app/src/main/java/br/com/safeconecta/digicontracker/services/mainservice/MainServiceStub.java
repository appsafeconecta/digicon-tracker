package br.com.safeconecta.digicontracker.services.mainservice;


import android.util.Log;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import br.com.safeconecta.digicontracker.clientmessages.TrackedLocation;

/**
 * Created by Rafael on 17/02/2017.
 */

public class MainServiceStub implements IMainService {

    private static final String TAG = "MainServiceStub";
    private int nTracking = 0;

    @Override
    public ServerResponse sendLocation(TrackedLocation trackedLocation) {
        //String imei = "356631060138759";
        DecimalFormat coordinatesFormat = new DecimalFormat("+#0.000000;-#", new DecimalFormatSymbols(Locale.US));
        DecimalFormat speedFormat = new DecimalFormat("#0.000", new DecimalFormatSymbols(Locale.US));
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        DateFormat timeFormat = new SimpleDateFormat("HHmmss");

        String deviceId = "060138759";
        Date currentDate = new Date();
        String date = dateFormat.format(currentDate);
        String time = timeFormat.format(currentDate);
        String latitude = coordinatesFormat.format(trackedLocation.getLatitude());
        String longitude = coordinatesFormat.format(trackedLocation.getLongitude());
        String speed = speedFormat.format(trackedLocation.getSpeed());
        String numMessage = String.format(Locale.US, "%04d", nTracking);
        String trackingString = "SCDC01;1;" + deviceId + ";" + numMessage + ";POS01;" + date + ";" + time + ";" + latitude + ";" + longitude + ";" + speed + ";FF;FF;0;0.00;0;";
        String checkedString = trackingString + getChecksum(trackingString);
        sendToService(checkedString);
        nTracking++;
        if(nTracking > 9999)
            nTracking = 0;

        int previousDistance = (int) (Math.random() * 100) + 50;
        int nextDistance = (int) (Math.random() * 100) + 50;
        int previousTime = (int) (Math.random() * 2400) + 600;
        int nextTime = (int) (Math.random() * 2400) + 600;
        String response = "RI@CENTRO - IDA - VIA LB/PR/AT/CT@00552A#NC@ABC@" +
                previousDistance + "@" +
                previousTime + "@DEF@" +
                nextDistance + "@" +
                nextTime + "#AI@TERMINAL FORUM@827";
        return new ServerResponse(response);
    }

    @Override
    public AuthenticationResponse authenticate(String register) {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if ("123".equals(register))
            return new AuthenticationResponse(true);
        else
            return new AuthenticationResponse("João da Silva");
    }

    @Override
    public void startDrive(String register, String imei) {
        System.out.println("Start Drive");
    }

    private void sendToService(String messageStr) {
        try {
            System.out.println("Trying to send: " + messageStr);
            Log.d("sendToService", "Trying to send: " + messageStr);
            int serverPort = 5610;
            DatagramSocket socket = new DatagramSocket();
            InetAddress server = InetAddress.getByName("hubscdc.gpsconecta.com.br");
            int messageLength = messageStr.length();
            byte[] message = messageStr.getBytes();
            DatagramPacket packet = new DatagramPacket(message, messageLength, server, serverPort);
            socket.send(packet);

            socket.setSoTimeout(10000);
            while(true) {
                byte[] response = new byte[256];
                DatagramPacket responsePacket = new DatagramPacket(response, response.length);
                try {
                    socket.receive(responsePacket);
                } catch(SocketTimeoutException ex) {
                    // Resend packet
                    socket.send(packet);
                    continue;
                }
                String received = new String(responsePacket.getData(), 0, responsePacket.getLength());
                System.out.println("Received: "+ received);
                Log.d("sendToService", "Received: "+ received);
                break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getChecksum(String msg) {
        int checksum = 0;
        char[] msgArray = msg.toCharArray();
        for(char ch : msgArray) {
            checksum = checksum ^ ((int)ch);
        }
        return String.format("%2s", Integer.toHexString(checksum).toUpperCase(Locale.US)).replace(' ', '0');
    }
}
