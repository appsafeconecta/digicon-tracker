package br.com.safeconecta.digicontracker.clientmessages;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Rafael on 17/02/2017.
 */

public class TrackedLocation {

    private static final String TAG = "TrackedLocation";

    private String imei;
    private String jsonString;
    private double latitude;
    private double longitude;
    private double speed;

    public TrackedLocation(Date date, String imei, double latitude, double longitude, float speed, float bearing) {
        this.imei = imei;
        this.latitude = latitude;
        this.longitude = longitude;
        this.speed = speed;
        JSONObject jsonObject = new JSONObject();
        DateFormat dt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        try {
            jsonObject.put("DateTime", dt.format(date));
            jsonObject.put("IMEI", imei);
            jsonObject.put("Latitude", latitude);
            jsonObject.put("Longitude", longitude);
            jsonObject.put("Speed", speed);
            jsonObject.put("Bearing", bearing);
            jsonString = jsonObject.toString();
        } catch(JSONException e){
            Log.e(TAG, "Failed to write json", e);
        }
    }

    public String getImei() {
        return imei;
    }

    public String getJsonString() {
        return jsonString;
    }

    public double getLatitude() { return latitude; }

    public double getLongitude() { return longitude; }

    public double getSpeed() { return speed; }
}
