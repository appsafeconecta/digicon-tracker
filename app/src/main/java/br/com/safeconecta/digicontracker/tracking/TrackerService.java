package br.com.safeconecta.digicontracker.tracking;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import br.com.safeconecta.digicontracker.R;
import br.com.safeconecta.digicontracker.activities.InitialActivity;


/**
 * Created by Rafael on 17/02/2017.
 */

public class TrackerService extends Service {

    private static final String TAG = "TrackerService";
    private static final int LOCATION_INTERVAL = 100;
    private static final float LOCATION_DISTANCE = 3f;

    private Handler handler;
    private LocationManager locationManager;
    private TrackerThread trackerThread;

    private TrackerLocationListener locationListener = new TrackerLocationListener(LocationManager.GPS_PROVIDER);

    private class TrackerLocationListener implements LocationListener {

        private String provider;
        private Location lastLocation;

        private TrackerLocationListener(String provider) {
            this.provider = provider;
            lastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(Location location) {
            Log.d(TAG, "onLocationChanged " + location.getLatitude() + ", " + location.getLongitude());
            lastLocation = location;
            trackerThread.setLocation(lastLocation);
        }

        @Override
        public void onProviderEnabled(String s) {
            Log.d(TAG, "onProviderEnabled " + s);
            if (LocationManager.GPS_PROVIDER.equals(s)) {
                handler.post(new DisplayToast(getApplicationContext(), R.string.gps_enabled));
                // TODO send message to web service informing about GPS enabled
                Log.w(TAG, "GPS enabled again");

                try {
                    lastLocation = locationManager.getLastKnownLocation(provider);
                    trackerThread.setLocation(lastLocation);
                } catch (SecurityException e) {
                    Log.e(TAG, "Failed to get last known location", e);
                }
            }
        }

        @Override
        public void onProviderDisabled(String s) {
            Log.d(TAG, "onProviderDisabled " + s);
            if (LocationManager.GPS_PROVIDER.equals(s)) {
                handler.post(new DisplayToast(getApplicationContext(), R.string.gps_disabled));
                // TODO send message to web service warning about GPS disabled
                Log.w(TAG, "Warning: GPS disabled!");
            }
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
            Log.d(TAG, "onStatusChanged " + s);
        }

    }

    private final IBinder binder = new TrackerBinder();

    public class TrackerBinder extends Binder {
        public TrackerService getService() {
            return TrackerService.this;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);
        //handler.post(new DisplayToast(getApplicationContext(), R.string.service_started));

        try {
            Location lastLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            trackerThread.setLocation(lastLocation);
        } catch (SecurityException e) {
            Log.e(TAG, "Fail to get last known location", e);
        }

        return START_STICKY;
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate");
        super.onCreate();

        Intent notificationIntent = new Intent(this, InitialActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        Notification notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getString(R.string.app_name))
                .setContentIntent(pendingIntent).build();
        startForeground(1001, notification);

        trackerThread = new TrackerThread(getApplicationContext());
        trackerThread.start();

        handler = new Handler();

        initializeLocationManager();

        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE, locationListener);
        } catch (SecurityException e) {
            Log.e(TAG, "Fail to request location update, ignore", e);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, "GPS provider does not exist " + e.getMessage());
        }
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        handler.post(new DisplayToast(getApplicationContext(), R.string.service_destroyed));
        super.onDestroy();

        // TODO send message to web service warning about service not running

        trackerThread.interrupt();
        if (locationManager != null) {
            try {
                locationManager.removeUpdates(locationListener);
            } catch (SecurityException e) {
                Log.e(TAG, "Fail to remove location listeners, ignore", e);
            }
        }
    }

    private void initializeLocationManager() {
        Log.d(TAG, "initializeLocationManager");
        if (locationManager == null) {
            locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }

    public void requestLocationUpdatesAgain() {
        Log.d(TAG, "Requesting location updates again");
        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE, locationListener);
            Location lastLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            trackerThread.setLocation(lastLocation);
        } catch (SecurityException e) {
            Log.e(TAG, "Fail to request location update again, ignore", e);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, "GPS provider does not exist " + e.getMessage());
        }
    }

    private class DisplayToast implements Runnable {
        private final Context context;
        private int resId;

        private DisplayToast(Context context, int resId) {
            this.context = context;
            this.resId = resId;
        }

        @Override
        public void run() {
            Toast.makeText(context, resId, Toast.LENGTH_SHORT).show();
        }
    }
}
