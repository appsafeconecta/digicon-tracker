package br.com.safeconecta.digicontracker.servermessages;

/**
 * Created by Rafael on 20/02/2017.
 */

public class AccessibilityInfo extends ServerMessage {

    private String stop;
    private int time;

    public AccessibilityInfo(String stop, int time) {
        super(TYPE_ACCESSIBILITY_INFO);
        this.stop = stop;
        this.time = time;
    }

    public String getStop() {
        return stop;
    }

    public int getTime() { return time; }
}
