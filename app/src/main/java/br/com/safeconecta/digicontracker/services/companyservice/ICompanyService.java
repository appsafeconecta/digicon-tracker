package br.com.safeconecta.digicontracker.services.companyservice;

/**
 * Created by Rafael on 04/04/2017.
 */

public interface ICompanyService {

    public CompanyResponse getCompanyParameters(String code);
}
