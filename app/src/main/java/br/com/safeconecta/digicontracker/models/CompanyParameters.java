package br.com.safeconecta.digicontracker.models;

/**
 * Created by Rafael on 04/04/2017.
 */

public class CompanyParameters {

    private String name;
    private boolean authentication;

    public CompanyParameters(String name, boolean authentication) {
        this.name = name;
        this.authentication = authentication;
    }

    public String getName() {
        return name;
    }

    public boolean authenticationFlag() {
        return authentication;
    }
}
