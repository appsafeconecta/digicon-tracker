package br.com.safeconecta.digicontracker.services.mainservice;

import android.util.Log;

import org.json.JSONException;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import br.com.safeconecta.digicontracker.clientmessages.TrackedLocation;

/**
 * Created by Rafael on 17/02/2017.
 */

public class MainService implements IMainService {

    private static final String TAG = "MainService";

    @Override
    public ServerResponse sendLocation(TrackedLocation trackedLocation) {
        HttpsURLConnection urlConnection;
        ServerResponse response;
        try {
            URL url = new URL("https://gpstracker-9c10b.firebaseio.com/UserTrack/" + trackedLocation.getImei() + ".json");
            urlConnection = (HttpsURLConnection) url.openConnection();
            try {
                urlConnection.setConnectTimeout(5000);
                urlConnection.setReadTimeout(5000);
                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

                OutputStream outputStream = urlConnection.getOutputStream();
                outputStream.write(trackedLocation.getJsonString().getBytes("UTF-8"));
                outputStream.close();

                InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                response = new ServerResponse(readStream(inputStream));
            } finally {
                urlConnection.disconnect();
            }
        } catch (Exception e) {
            Log.e(TAG, "Error: " + e.getMessage());
            response = new ServerResponse(true);
        }

        return response;
    }

    private String readStream(InputStream inputStream) throws IOException, JSONException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder total = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            total.append(line).append('\n');
        }
        return total.toString();
    }

    @Override
    public AuthenticationResponse authenticate(String register) {
        return new AuthenticationResponse(true);
    }

    @Override
    public void startDrive(String register, String imei) {
        // TODO send to service async
    }
}
