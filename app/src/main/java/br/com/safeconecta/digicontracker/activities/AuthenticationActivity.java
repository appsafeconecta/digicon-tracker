package br.com.safeconecta.digicontracker.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import br.com.safeconecta.digicontracker.Properties;
import br.com.safeconecta.digicontracker.R;
import br.com.safeconecta.digicontracker.models.CompanyParameters;
import br.com.safeconecta.digicontracker.models.DriverInfo;
import br.com.safeconecta.digicontracker.services.mainservice.AuthenticationResponse;
import br.com.safeconecta.digicontracker.services.mainservice.IMainService;
import br.com.safeconecta.digicontracker.services.mainservice.MainServiceStub;

/**
 * Created by Rafael on 04/04/2017.
 */

public class AuthenticationActivity extends AppCompatActivity {

    private static final String TAG = "AuthenticationActivity";

    private CompanyParameters companyParameters;

    private EditText etRegister;
    private Button btAuthenticate;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_authentication);

        companyParameters = Properties.readObject(this, getString(R.string.prefs_company_parameters), CompanyParameters.class);

        etRegister = (EditText) findViewById(R.id.etRegister);
        etRegister.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                btAuthenticate.setEnabled(s.length() > 0);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btAuthenticate = (Button) findViewById(R.id.btAuthenticate);
    }

    public void authenticate(View view) {
        btAuthenticate.setEnabled(false);
        final ProgressDialog progressDialog = ProgressDialog.show(this, getText(R.string.authenticating_title), getText(R.string.authenticating_message));
        progressDialog.setCanceledOnTouchOutside(false);

        Thread thread = new Thread() {
            @Override
            public void run() {
                final String register = etRegister.getText().toString();
                final IMainService mainService = new MainServiceStub();
                final AuthenticationResponse response = mainService.authenticate(register);
                if (response.isError()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(AuthenticationActivity.this)
                                    .setTitle(R.string.error)
                                    .setMessage(R.string.authentication_failed_dialog)
                                    .setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    })
                                    .show();
                            btAuthenticate.setEnabled(true);
                            progressDialog.dismiss();
                        }
                    });
                } else {
                    final DriverInfo driverInfo = new DriverInfo(register, response.getDriverName());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(AuthenticationActivity.this)
                                    .setTitle(R.string.authentication_confirmation_title)
                                    .setMessage(getString(R.string.authentication_confirmation_message, driverInfo.getName()))
                                    .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                            btAuthenticate.setEnabled(true);
                                            progressDialog.dismiss();
                                        }
                                    })
                                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Properties.writeObject(AuthenticationActivity.this, getString(R.string.prefs_driver_info), driverInfo);
                                            dialog.cancel();
                                            progressDialog.dismiss();
                                            finish();
                                        }
                                    })
                                    .show();
                        }
                    });

                }
            }
        };
        thread.start();
    }
}
