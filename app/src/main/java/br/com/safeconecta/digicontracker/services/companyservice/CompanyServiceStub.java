package br.com.safeconecta.digicontracker.services.companyservice;


import br.com.safeconecta.digicontracker.models.CompanyParameters;

/**
 * Created by Rafael on 04/04/2017.
 */

public class CompanyServiceStub implements ICompanyService {

    @Override
    public CompanyResponse getCompanyParameters(String code) {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if ("RIOITA".equals(code)) {
            CompanyParameters parameters = new CompanyParameters("Rio Ita", true);
            return new CompanyResponse(parameters);
        } else if ("NOVACAP".equals(code)) {
            CompanyParameters parameters = new CompanyParameters("Novacap", false);
            return new CompanyResponse(parameters);
        }

        return new CompanyResponse(false);
    }
}
