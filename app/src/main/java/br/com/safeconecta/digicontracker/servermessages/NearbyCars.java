package br.com.safeconecta.digicontracker.servermessages;

/**
 * Created by Rafael on 17/02/2017.
 */

public class NearbyCars extends ServerMessage {

    private String previousId;
    private int previousDistance;
    private int previousTime;
    private String nextId;
    private int nextDistance;
    private int nextTime;

    public NearbyCars(String previousId, int previousDistance, int previousTime, String nextId, int nextDistance, int nextTime) {
        super(TYPE_NEARBY_CARS);

        this.previousId = previousId;
        this.previousDistance = previousDistance;
        this.previousTime = previousTime;
        this.nextId = nextId;
        this.nextDistance = nextDistance;
        this.nextTime = nextTime;
    }

    public String getPreviousId() {
        return previousId;
    }

    public int getPreviousDistance() {
        return previousDistance;
    }

    public int getPreviousTime() {
        return previousTime;
    }

    public String getNextId() {
        return nextId;
    }

    public int getNextDistance() {
        return nextDistance;
    }

    public int getNextTime() {
        return nextTime;
    }
}
