package br.com.safeconecta.digicontracker.activities;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import br.com.safeconecta.digicontracker.Properties;
import br.com.safeconecta.digicontracker.R;
import br.com.safeconecta.digicontracker.customviews.NearbyMap;
import br.com.safeconecta.digicontracker.models.CompanyParameters;
import br.com.safeconecta.digicontracker.models.DriverInfo;
import br.com.safeconecta.digicontracker.servermessages.NearbyCars;
import br.com.safeconecta.digicontracker.servermessages.ServerMessage;
import br.com.safeconecta.digicontracker.services.mainservice.ServerResponse;
import br.com.safeconecta.digicontracker.tracking.TrackerService;

/**
 * Created by Rafael on 04/04/2017.
 */

public class TrackingActivity extends AppCompatActivity {

    private static final String TAG = "TrackingActivity";
    public static final int PERMISSIONS_LOCATION = 0;

    private TrackerService trackerService;
    private boolean bound = false;
    private CompanyParameters companyParameters;
    private DriverInfo driverInfo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_tracking);

        List<String> permissionsToRequest = new ArrayList<>();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissionsToRequest.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            permissionsToRequest.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            permissionsToRequest.add(Manifest.permission.READ_PHONE_STATE);
        }
        if (permissionsToRequest.size() > 0) {
            ActivityCompat.requestPermissions(this, permissionsToRequest.toArray(new String[permissionsToRequest.size()]), PERMISSIONS_LOCATION);
        }


        companyParameters = Properties.readObject(this, getString(R.string.prefs_company_parameters), CompanyParameters.class);

        TextView tvCompanyName = (TextView) findViewById(R.id.tvCompanyName);
        tvCompanyName.setText(companyParameters.getName());
    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent trackerServiceIntent = new Intent(this, TrackerService.class);
        startService(trackerServiceIntent);
        bindService(trackerServiceIntent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (bound) {
            unbindService(serviceConnection);
            bound = false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.d(TAG, "Registering broadcast receiver");
        IntentFilter intentFilter = new IntentFilter(getString(R.string.broadcast_action_key));
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, intentFilter);

        if (companyParameters.authenticationFlag()) {
            driverInfo = Properties.readObject(this, getString(R.string.prefs_driver_info), DriverInfo.class);
            if (driverInfo == null) {
                Button btOpenAuthentication = (Button) findViewById(R.id.btOpenAuthentication);
                btOpenAuthentication.setVisibility(View.VISIBLE);
            } else {
                TextView tvDriverName = (TextView) findViewById(R.id.tvDriverName);
                tvDriverName.setText(driverInfo.getName());
                Button btStartDrive = (Button) findViewById(R.id.btStartDrive);
                btStartDrive.setVisibility(View.VISIBLE);
                Button btOpenAuthentication = (Button) findViewById(R.id.btOpenAuthentication);
                btOpenAuthentication.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        Log.d(TAG, "Unregistering broadcast receiver");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Map<String, Integer> perms = new HashMap<String, Integer>();
        // Initial
        perms.put(Manifest.permission.ACCESS_COARSE_LOCATION, PackageManager.PERMISSION_GRANTED);
        perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
        perms.put(Manifest.permission.READ_PHONE_STATE, PackageManager.PERMISSION_GRANTED);
        // Fill with results
        for (int i = 0; i < permissions.length; i++)
            perms.put(permissions[i], grantResults[i]);
        // Check for ACCESS_FINE_LOCATION
        if (perms.get(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && perms.get(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            // All Permissions Granted
            Log.d(TAG, "Permissions granted");
            if (bound) {
                trackerService.requestLocationUpdatesAgain();
            }
        } else {
            // Permission Denied
            Log.w(TAG, "Warning: Permissions are not granted!");
            finish();
        }
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            TrackerService.TrackerBinder binder = (TrackerService.TrackerBinder) iBinder;
            trackerService = binder.getService();
            bound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            bound = false;
        }
    };

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ServerResponse response = new ServerResponse(intent.getStringExtra(getString(R.string.data_main_service_response_key)));
            for (ServerMessage msg : response.getMessages()) {
                Log.d(TAG, "msg type: " + msg.getType());
                if (ServerMessage.TYPE_NEARBY_CARS == msg.getType()) {
                    NearbyCars nearbyCars = (NearbyCars) msg;
                    float ratio = (float) nearbyCars.getPreviousTime() / (nearbyCars.getPreviousTime() + nearbyCars.getNextTime());
                    Log.d(TAG, "ratio: " + String.format(Locale.US, "%2f", ratio));
                    NearbyMap nearbyMap = (NearbyMap) findViewById(R.id.nearbyMap);
                    nearbyMap.setRatio(ratio);
                }
            }
        }
    };

    public void openAuthentication(View view) {
        startActivity(new Intent(this, AuthenticationActivity.class));
    }

    public void startDrive(View view) {
    }

    public void toggleDirection(View view) {
        NearbyMap nearbyMap = (NearbyMap) findViewById(R.id.nearbyMap);
        nearbyMap.toggleDirection();
    }
}
